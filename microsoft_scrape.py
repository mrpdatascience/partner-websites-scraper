# -*- coding: utf-8 -*-
"""
Created on Mon Apr 24 08:58:53 2017

@author: kdalal
"""

import requests
import simplejson
import pandas as pd

#Importing all country codes
con_code = pd.read_csv("country.csv",header=None)
con_code = con_code.ix[:,0].tolist()

header = {
        'Accept':'application/json, text/plain, */*',
        'Accept-Encoding':'gzip, deflate, br',
        'Accept-Language':'en-US,en;q=0.8',
        'Authorization':'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6InowMzl6ZHNGdWl6cEJmQlZLMVRuMjVRSFlPMCIsImtpZCI6InowMzl6ZHNGdWl6cEJmQlZLMVRuMjVRSFlPMCJ9.eyJhdWQiOiJodHRwczovL2FwaS5wYXJ0bmVyY2VudGVyLm1pY3Jvc29mdC5jb20iLCJpc3MiOiJodHRwczovL3N0cy53aW5kb3dzLm5ldC83MmY5ODhiZi04NmYxLTQxYWYtOTFhYi0yZDdjZDAxMWRiNDcvIiwiaWF0IjoxNDkzNjQ5NDg5LCJuYmYiOjE0OTM2NDk0ODksImV4cCI6MTQ5MzY1MzM4OSwiYWlvIjoiWTJaZ1lNaG5ucjR4cWxUd2ZYeGRoVUJBNElrSEFBPT0iLCJhcHBpZCI6ImFhNzVmYTU1LWNmZWEtNDc0MS1iZWI4LTRjMjZmMmE2NzljZiIsImFwcGlkYWNyIjoiMSIsImVfZXhwIjoyNjI4MDAsImlkcCI6Imh0dHBzOi8vc3RzLndpbmRvd3MubmV0LzcyZjk4OGJmLTg2ZjEtNDFhZi05MWFiLTJkN2NkMDExZGI0Ny8iLCJvaWQiOiI4OTk3ZGYxNC1jZjlkLTRjMmYtYTlkNy03NDQyM2I2ZGUxYjQiLCJzdWIiOiI4OTk3ZGYxNC1jZjlkLTRjMmYtYTlkNy03NDQyM2I2ZGUxYjQiLCJ0ZW5hbnRfcmVnaW9uX3Njb3BlIjoiV1ciLCJ0aWQiOiI3MmY5ODhiZi04NmYxLTQxYWYtOTFhYi0yZDdjZDAxMWRiNDciLCJ2ZXIiOiIxLjAifQ.fMHc4R133olzZLZEpZ3ZgodsKveOCBoHkLt2JQmil-qV7NkmKsEyRUUo3LDYm8A-afj6Zz3FkehI7qgbVxnztHh201ivoVLpz2HN3UpFbxEydDorxyTm23PjjTaeN8o-v_zb7blongGgaAY9xdSq3jVsNHO3-pv-CBmUaEzCD5Dti2RUsAq7WyQeVjzSsykab2DLHPyMXlgiWf6TW6X7F67YBXtfBi8MdzXsNGlljDzWiz791HAb6hbAP170ybaDc7h41i0tUd1Wua5SN4yQI6Va6erfUuGy2-e9Yv7OxHocLosDl7XjheoEu-08ePXdXEws3xkIWPnGJlEDkZdN6g',
        'Cache-Control':'no-cache',
        'Connection':'keep-alive',
        'Content-Length':'196',
        'Content-Type':'application/json;charset=UTF-8',
        'Expires':'-1',
        'Host':'api.partnercenter.microsoft.com',
        'MS-CorrelationId':'8b4eb0f1-7879-4dd9-b3eb-ae2e66c63831',
        'MS-PartnerCenter-Client':'Partner Center Web',
        'MS-RequestId':'ea47eb4b-5448-46df-cbfc-5df036f8839a',
        'ocp-apim-subscription-key':'c306f5dd740f4946920822865932a356',
        'Origin':'https://partnercenter.microsoft.com',
        'Pragma':'no-cache',
        'Referer':'https://partnercenter.microsoft.com/en-us/pcv/search',
        'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:21.0) Gecko/20100101 Firefox/21.0',
        'X-Locale':'en-US',
        'x-searchTrackingId':'012a1045-7b91-4f75-e77f-ee85a4302166',
        'x-sessionId':'2052a6bc-7664-45f3-9ef5-5be9ecb5cd7a'
        }

#m_items = []
#for i in range(8200,15000,20):
#    payload = {"Keyword":"*","Size":20,"Offset":i,"IndustryFocus":'',"Product":'',"ServiceType":'',"GeoRadius":'',"Latitude":'',"Longitude":'',"CustomFilters":'',"SeatCount":0,"CountryCode":''}
#    print payload['Offset']
#    m_result = requests.post("https://api.partnercenter.microsoft.com/partnerfinder/v1/marketingprofiles",data=simplejson.dumps(payload),headers=header)
#    m_content = simplejson.loads(str(m_result.content))
#    m_items.append(m_content['matchingPartners']['items'])
#

n=[]
for i in ['US']:
    for j in range(0,1500,20):
        payload = {"Keyword":"*","Size":20,"Offset":j,"IndustryFocus":'',"Product":'',"ServiceType":'',"GeoRadius":'',"Latitude":'',"Longitude":'',"CustomFilters":'',"SeatCount":0,"CountryCode":i}
        print payload
        try:            
            p = requests.post("https://api.partnercenter.microsoft.com/partnerfinder/v1/marketingprofiles",data=simplejson.dumps(payload),headers=header)
            m = simplejson.loads(str(p.content))
            n.append(m['matchingPartners']['items'])
            print len(n)
        except Exception:
            pass

m_extract = []
for i in n:
    m_extract += i 
  
company = [i['name'] for i in m_extract]
partner_id = [i['partnerId'] for i in m_extract]
location = [i['location'] for i in m_extract]
address = [i['address'] for i in location]
address1 = [i.get('addressLine1') for i in address]
address2 = [i.get('addressLine2') for i in address]
city = [i.get('city') for i in address]
state = [i.get('state') for i in address]
zipcode = [i.get('postalCode') for i in address]
country = [i.get('country') for i in address]
industryFocus = [i['industryFocus'] for i in m_extract]
#serviceType = [i['serviceType'] for i in m_extract] 
#product = [i['product'] for i in m_extract]

microsoft = pd.DataFrame(company, columns=['company'])
microsoft['partner_id'] = partner_id
microsoft['address1'] = address1
microsoft['address2'] = address2
microsoft['city'] = city
microsoft['state'] = state
microsoft['zipcode'] = zipcode
microsoft['country'] = country
microsoft['industryFocus'] = industryFocus
#microsoft['serviceType'] = serviceType
#microsoft['product'] = product
microsoft.to_csv('microsoft1.csv',encoding='utf-8')
