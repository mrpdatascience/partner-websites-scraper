# -*- coding: utf-8 -*-
"""
Created on Mon Apr 17 12:19:24 2017

@author: kdalal
"""

import requests
from bs4 import BeautifulSoup
import ssl
import simplejson
import pandas as pd

ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

#url = "https://partnerlocator.vmware.com/#first=0&sort=%40distance%20ascending&fq=y&clchk=n%7CUNITED%20STATES%7C%7C%7C37.09024%7C-95.712891&cmp=Business%20Continuity"

#constructing urls for location and business competency
def build_urls(url,total_count):
    
    new_urls=[]
    i=0 
    while i < total_count:
        a = list(url)
        a[41]=str(i)
        a = "".join(a)
        i += 10
        new_urls.append(a)
    return new_urls

#Grabbing all hrefs from each page
def grab_href(url):
    
    hrefs=[]
    page = requests.get(url,timeout=5,verify=False)
    soup = BeautifulSoup(page.content,"html.parser")
    for link in soup.findAll('a'):
        b = link.get('href')
        hrefs.append(b)
    return hrefs 

header = {'Accept':'*/*',
          'Accept-Encoding':'gzip, deflate, br',
          'Accept-Language':'en-US,en;q=0.8',
          'Authorization':'Bearer eyJhbGciOiJIUzI1NiJ9.eyJmaWx0ZXIiOiJAb2JqZWN0dHlwZT1hY2NvdW50IEBzZmFjY291bnRpc3BhcnRuZXJhY3RpdmVjPTEgQHBsb2NwYXJ0bmVydHlwZSBAcGxvY3BhcnRuZXJsZXZlbCBAc2Zvcmdhbml6YXRpb25pZD0wMEQ0MDAwMDAwMDloUVIiLCJ1c2VyR3JvdXBzIjpbIlBhcnRuZXIgTG9jYXRvciBQcm9maWxlIl0sInY4IjpmYWxzZSwib3JnYW5pemF0aW9uIjoidm13YXJlIiwidXNlcklkcyI6W3sicHJvdmlkZXIiOiJFbWFpbCBTZWN1cml0eSBQcm92aWRlciIsIm5hbWUiOiJhbm9ueW1vdXMiLCJ0eXBlIjoiVXNlciJ9LHsicHJvdmlkZXIiOiJFbWFpbCBTZWN1cml0eSBQcm92aWRlciIsIm5hbWUiOiJwYXJ0bmVyY2VudHJhbEB2bXdhcmUuY29tIiwidHlwZSI6IlVzZXIifSx7InByb3ZpZGVyIjoiRW1haWwgU2VjdXJpdHkgUHJvdmlkZXIiLCJuYW1lIjoianBkZXJ5QGNvdmVvLmNvbSIsInR5cGUiOiJVc2VyIn1dLCJyb2xlcyI6WyJxdWVyeUV4ZWN1dG9yIl0sImV4cCI6MTQ5Mzc0MjkzNSwiaWF0IjoxNDkzNjU2NTM1fQ.ayUElvat9Ctgg64cahkN9LAjnOUqj2lPkxSvlL1VTpc',
          'Connection':'keep-alive',
          'Content-Length':'2067',
          'Content-Type':'application/x-www-form-urlencoded; charset="UTF-8"',
          'Host':'cloudplatform.coveo.com',
          'Origin':'https://partnerlocator.vmware.com',
          'Referer':'https://partnerlocator.vmware.com/',
          'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:21.0) Gecko/20100101 Firefox/21.0'
            }    
  
   
states = ['CA','TX','NY','FL','IL','MA','VA','PA','NJ','GA','MD','OH','MI','CO','MN','NC','WI','AZ','IN','MO','CT','WA','TN','KS','LA','UT','IA','OR','SC','AL','KY','NE','NH',
          'NV','OK']
    
json_object=[]
for i in states:
    data = {'aq':'(@sfaccountcountryc=="UNITED STATES") (@sfaccountstateprovincec=='+i+') @distancemi<3500',
            'searchHub':'default',
            'language':'en',
            'wildcards':'true',
            'firstResult':'0',
            'numberOfResults':'1000',
            'excerptLength':'200',
            'filterField':'',
            'enableDidYouMean':'false',
            'sortCriteria':'fieldascending',
            'sortField':'@distance',
            'queryFunctions':'[{"function":"dist(@sfaccountlatitudedoublec, @sfaccountlongitudedoublec, 37.09024, -95.712891)","fieldName":"distance"},{"function":"@distance/1000","fieldName":"distanceKM"},{"function":"@distance*0.000621371","fieldName":"distanceMI"}]',
            'rankingFunctions':'[]',
            'groupBy':'[{"field":"@plocpartnertype","maximumNumberOfValues":6,"sortCriteria":"occurrences","injectionDepth":1000,"completeFacetWithStandardValues":true},{"field":"@plocpartnerlevel","maximumNumberOfValues":6,"sortCriteria":"occurrences","injectionDepth":1000,"completeFacetWithStandardValues":true},{"field":"@plocpurchasingprograms","maximumNumberOfValues":7,"sortCriteria":"nosort","injectionDepth":1000,"completeFacetWithStandardValues":true,"allowedValues":["VPP","EPP","HPP","SPPP","SPPM","SDP"]},{"field":"@sfaccountcountryc","maximumNumberOfValues":6,"sortCriteria":"occurrences","injectionDepth":1000,"completeFacetWithStandardValues":true},{"field":"@sfaccountstateprovincec","maximumNumberOfValues":6,"sortCriteria":"occurrences","injectionDepth":1000,"completeFacetWithStandardValues":true}]',
            'retrieveFirstSentences':'true',
            'timezone':'America/New_York',
            'enableDuplicateFiltering':'false',
            'enableCollaborativeRating':'false'
            }
    print data['aq']
    page = requests.post('https://cloudplatform.coveo.com/rest/search/?errorsAsSuccess=1',data,headers=header)
    json_temp = simplejson.loads(str(page.content))
    json_object.append(json_temp["results"])

json_list=[]
for i in json_object:
        json_list = json_list + i

geoloc_ind = {'CANADA':['37.09024','-95.712891'],
            'MEXICO':['37.09024','-95.712891'],
            'COLOMBIA':['37.09024','-95.712891'],
            'ECUADOR':['37.09024','-95.712891'],
            'PANAMA':['37.09024','-95.712891'],
            'GUATEMALA':['37.09024','-95.712891'],
            'ICELAND':['37.09024','-95.712891'],
            'NICARAGUA':['37.09024','-95.712891'],
            'BERMUDA':['37.09024','-95.712891'],
            'HONDURAS':['37.09024','-95.712891'],
            'JAMAICA':['37.09024','-95.712891'],
            'CURACAO':['37.09024','-95.712891'],
            'PERU':['37.09024','-95.712891'],
            'GERMANY':['55.378051','-3.435973'],
            'ITALY':['55.378051','-3.435973'],
            'FRANCE':['55.378051','-3.435973'],
            'NETHERLANDS':['55.378051','-3.435973'],
            'SPAIN':['55.378051','-3.435973'],
            'SWITZERLAND':['55.378051','-3.435973'],
            'POLAND':['55.378051','-3.435973'],
            'SWEDEN':['55.378051','-3.435973'],
            'AUSTRIA':['55.378051','-3.435973'],
            'BELGIUM':['55.378051','-3.435973'],
            'TURKEY':['55.378051','-3.435973'],
            'NORWAY':['55.378051','-3.435973'],
            'DENMARK':['55.378051','-3.435973'],
            'FINLAND':['55.378051','-3.435973'],
            'PORTUGAL':['55.378051','-3.435973'],
            'ROMANIA':['55.378051','-3.435973'],
            'IRELAND':['55.378051','-3.435973'],
            'HUNGARY':['55.378051','-3.435973'],
            'ISRAEL':['55.378051','-3.435973'],
            'EGYPT':['55.378051','-3.435973'],
            'GREECE':['55.378051','-3.435973'],
            'SLOVAKIA':['55.378051','-3.435973'],
            'NIGERIA':['55.378051','-3.435973'],
            'SLOVENIA':['55.378051','-3.435973'],
            'MOROCCO':['55.378051','-3.435973'],
            'UKRAINE':['55.378051','-3.435973'],
            'LUXEMBOURG':['55.378051','-3.435973'],
            'BULGARIA':['55.378051','-3.435973'],
            'CROATIA':['55.378051','-3.435973'],
            'CYPRUS':['55.378051','-3.435973'],
            'KUWAIT':['55.378051','-3.435973'],
            'SERBIA':['55.378051','-3.435973'],
            'LEBANON':['55.378051','-3.435973'],
            'LITHUANIA':['55.378051','-3.435973'],
            'LATVIA':['55.378051','-3.435973'],
            'ALGERIA':['55.378051','-3.435973'],
            'TUNISIA':['55.378051','-3.435973'],
            'QATAR':['55.378051','-3.435973'],
            'ESTONIA':['55.378051','-3.435973'],
            'JORDAN':['55.378051','-3.435973'],
            'BELARUS':['55.378051','-3.435973'],
            'SENEGAL':['55.378051','-3.435973'],
            'GHANA':['55.378051','-3.435973'],
            'BAHRAIN':['55.378051','-3.435973'],
            'MALTA':['55.378051','-3.435973'],
            'GEORGIA':['55.378051','-3.435973'],
            'AZERBAIJAN':['55.378051','-3.435973'],
            'ALBANIA':['55.378051','-3.435973'],
            'IRAQ':['55.378051','-3.435973'],
            'MONACO':['55.378051','-3.435973'],
            'INDIA':['23.424076', '53.847818'],
            'PAKISTAN':['23.424076', '53.847818'],
            'BANGLADESH':['23.424076', '53.847818'],
            'OMAN':['23.424076', '53.847818'],
            'NEPAL':['23.424076', '53.847818'],
            'CAMBODIA':['23.424076', '53.847818'],
            'RWANDA':['23.424076', '53.847818'],
            'ZIMBABWE':['23.424076', '53.847818'],
            'UZBEKISTAN':['23.424076', '53.847818'],
            'AFGHANISTAN':['23.424076', '53.847818'],
            'TAJIKSTAN':['23.424076', '53.847818'],
            'TURKMENISTAN':['23.424076', '53.847818'],
            'YEMEN':['23.424076', '53.847818'],
            'SINGAPORE':['1.352083', '103.819836'],
            'JAPAN':['1.352083', '103.819836'],
            'CHINA':['1.352083', '103.819836'],
            'MALAYSIA':['1.352083', '103.819836'],
            'INDONESIA':['1.352083', '103.819836'],
            'THAILAND':['1.352083', '103.819836'],
            'TAIWAN':['1.352083', '103.819836'],
            'AUSTRALIA':['1.352083', '103.819836'],
            'KAZAKHSTAN':['1.352083', '103.819836'],
            'MYANMAR':['1.352083', '103.819836'],
            'MAURITIUS':['1.352083', '103.819836'],
            'MACAO':['1.352083', '103.819836'],
            'CHILE':['-23.442503','-58.443832'],
            'ARGENTINA':['-23.442503','-58.443832'],
            'URUGUAY':['-23.442503','-58.443832'],
            'PARAGUAY':['-23.442503','-58.443832']}

def countries(country,lati,longi):    
    
    result=[]    
    data = {'aq':'(@sfaccountcountryc=='+country+') (@distancemi<3500)',
            'searchHub':'default',
            'language':'en',
            'wildcards':'true',
            'firstResult':'0',
            'numberOfResults':'2000',
            'excerptLength':'200',
            'filterField':'',
            'enableDidYouMean':'false',
            'sortCriteria':'fieldascending',
            'sortField':'@distance',
            'queryFunctions':'[{"function":"dist(@sfaccountlatitudedoublec, @sfaccountlongitudedoublec,'+lati+','+longi+')","fieldName":"distance"},{"function":"@distance/1000","fieldName":"distanceKM"},{"function":"@distance*0.000621371","fieldName":"distanceMI"}]',
            'rankingFunctions':'[]',
            'groupBy':'[{"field":"@plocpartnertype","maximumNumberOfValues":6,"sortCriteria":"occurrences","injectionDepth":1000,"completeFacetWithStandardValues":true},{"field":"@plocpartnerlevel","maximumNumberOfValues":6,"sortCriteria":"occurrences","injectionDepth":1000,"completeFacetWithStandardValues":true},{"field":"@plocpurchasingprograms","maximumNumberOfValues":7,"sortCriteria":"nosort","injectionDepth":1000,"completeFacetWithStandardValues":true,"allowedValues":["VPP","EPP","HPP","SPPP","SPPM","SDP"]},{"field":"@sfaccountcountryc","maximumNumberOfValues":6,"sortCriteria":"occurrences","injectionDepth":1000,"completeFacetWithStandardValues":true},{"field":"@sfaccountstateprovincec","maximumNumberOfValues":6,"sortCriteria":"occurrences","injectionDepth":1000,"completeFacetWithStandardValues":true}]',
            'retrieveFirstSentences':'true',
            'timezone':'America/New_York',
            'enableDuplicateFiltering':'false',
            'enableCollaborativeRating':'false'
            }
    print data['aq']
    print data['queryFunctions']
    contents = requests.post('https://cloudplatform.coveo.com/rest/search/?errorsAsSuccess=1',data,headers=header)
    load = simplejson.loads(str(contents.content))
    print load['results'][0]
    result.append(load["results"])
    return result

temp=[]
for k,v in geoloc_ind.iteritems():
    try:
        temp.append(countries(k,v[0], v[1]))
    except Exception:
        pass
    
other_con = []
for j in temp:
    other_con = other_con + j

rest=[]
for j in other_con:
    rest = rest + j


geo_islands = {'SOUTH AFRICA':['-30.559482','22.937506'],
               'AMERICAN SAMOA':['14.3016396', '-170.6961815'],
                'ALAND ISLAND':['60.1785247', '19.9156105'],
                'ANTIGUA AND BARBADOS':['17.060816', '-61.796428'],
                'CZECH REPUBLIC':['49.81749199999999', '15.472962'],
                'CENTRAL AFRICAN':['6.611110999999999', '20.939444'],
                'BOSNIA AND HERZEGOVINA':['43.915886', '17.679076'],
                'COOK ISLANDS':['-21.236736','-159.777671'],
                'CAYMAN ISLANDS':['19.3133', '-81.2546'],
                'CAPE VERDE':['15.120142', '-23.6051868'],
                'BRUNEI DARUSSALAM':['4.535277','114.727669'],
                'COSTA RICA':['9.748916999999999','-83.753428'],
                'FAROE ISLANDS':['61.89263500000001','-6.911805999999999'],
                'DOMINICAN REPUBLIC':['18.735693','-70.162651'],
                'FRENCH GUIANA':['3.933889','-53.125782'],
                'FRENCH POLYNESIA':['-17.679742','-149.406843'],
                'FRENCH SOUTHERN':['-49.280366','69.348557'],
                'HONG KONG':['22.396428','114.109497'],
                'ISLE OF MAN':['54.236107','-4.548056'],
                'KOREA, REPUBLIC': ['35.907757','127.766922'],
                'LIBYAN ARAB JAMAHIRIYA':['26.3351','17.228331'],
                'MOLDOVA, REPUBLIC':['47.411631', '28.369885'],
                'NEW ZEALAND':['-40.900557','174.885971'],
                'PUERTO RICO':['18.220833', '-66.590149'],
                'SAUDI ARABIA':['23.885942', '45.079162'],
                'SOLOMON ISLANDS':['-9.64571', '160.156194'],
                'TRINIDAD AND TOBAGO': ['10.691803', '-61.222503'],
                'TANZANIA ISLAND':['-6.369028', '34.888822'],
                'UAE':['23.424076', '53.847818'],
                'UNITED KINGDOM':['55.378051','-3.435973'],
                'US MINOR ISLANDS':['19.2823192','166.647047'],
                'VIRGIN ISLANDS,BRITISH':['18.420695','-64.639968'],
                'VIRGIN ISLANDS, US':['18.335765','-64.896335'],
                'RUSSIAN FEDERATION':['61.52401','105.318756']}


def islands(latti,longi):
    r=[]    
    data = {'aq':'@distancemi<3500',
                'searchHub':'default',
                'language':'en',
                'wildcards':'true',
                'firstResult':'0',
                'numberOfResults':'2000',
                'excerptLength':'200',
                'filterField':'',
                'enableDidYouMean':'false',
                'sortCriteria':'fieldascending',
                'sortField':'@distance',
                'queryFunctions':'[{"function":"dist(@sfaccountlatitudedoublec, @sfaccountlongitudedoublec,'+latti+', '+longi+')","fieldName":"distance"},{"function":"@distance/1000","fieldName":"distanceKM"},{"function":"@distance*0.000621371","fieldName":"distanceMI"}]',
                'rankingFunctions':'[]',
                'groupBy':'[{"field":"@plocpartnertype","maximumNumberOfValues":6,"sortCriteria":"occurrences","injectionDepth":1000,"completeFacetWithStandardValues":true},{"field":"@plocpartnerlevel","maximumNumberOfValues":6,"sortCriteria":"occurrences","injectionDepth":1000,"completeFacetWithStandardValues":true},{"field":"@plocpurchasingprograms","maximumNumberOfValues":7,"sortCriteria":"nosort","injectionDepth":1000,"completeFacetWithStandardValues":true,"allowedValues":["VPP","EPP","HPP","SPPP","SPPM","SDP"]},{"field":"@sfaccountcountryc","maximumNumberOfValues":6,"sortCriteria":"occurrences","injectionDepth":1000,"completeFacetWithStandardValues":true},{"field":"@sfaccountstateprovincec","maximumNumberOfValues":6,"sortCriteria":"occurrences","injectionDepth":1000,"completeFacetWithStandardValues":true}]',
                'retrieveFirstSentences':'true',
                'timezone':'America/New_York',
                'enableDuplicateFiltering':'false',
                'enableCollaborativeRating':'false'
                }
    print data['queryFunctions']
    c = requests.post('https://cloudplatform.coveo.com/rest/search/?errorsAsSuccess=1',data,headers=header)
    l = simplejson.loads(str(c.content))
    print l['results'][0]
    r.append(l["results"])
    return r
    
u=[]
for val in geo_islands.itervalues():
    try:
        u.append(islands(val[0],val[1]))
    except Exception:
        pass

p=[]
for i in u:
    p=p+i
    
z=[]
for i in p:
    z=z+i     

combine = json_list + rest + z

company = [i['raw']['sfaccountname'] for i in combine]
partner_id = [i['raw']['sfaccountid'] for i in combine]
domain = [i['raw']['sfaccountprimarycontactemailcdomainname'] for i in combine]    
website = [i['raw'].get('sfaccountwebsite',None) for i in combine]
partner_type = [i['raw']['plocpartnertype'] for i in combine]
address1 = [i['raw'].get('sfaccountaddress1delc',None) for i in combine]
address2 = [i['raw'].get('sfaccountaddress2c',None) for i in combine]
city = [i['raw'].get('sfaccountcityc',None) for i in combine]
state = [i['raw'].get('sfaccountstateprovincec',None) for i in combine]
zipcode = [i['raw'].get('sfaccountzippostalcodeapexc',None) for i in combine]
country = [i['raw']['sfaccountcountryc'] for i in combine]
competency = [i['raw'].get('ploccompetencies', None) for i in combine]
industry = [i['raw'].get('sfindustry', None) for i in combine]
contact_email = [i['raw']['sfaccountprimarycontactemailformulac'] for i in combine]

vmware_df = pd.DataFrame(company,columns=['company'])
vmware_df['partnerid'] = partner_id
vmware_df['domain'] = domain
vmware_df['website'] = website
vmware_df['partner_type'] = partner_type
vmware_df['address1'] = address1
vmware_df['address2'] = address2
vmware_df['city'] = city
vmware_df['state'] = state
vmware_df['zipcode'] = zipcode
vmware_df['country'] = country
vmware_df['competency'] = competency
vmware_df['industry'] = industry
vmware_df['contact_email'] = contact_email          

vmware_df.to_csv('vmware.csv', encoding='utf-8')

