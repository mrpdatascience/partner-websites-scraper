# -*- coding: utf-8 -*-
"""
Created on Mon Apr 24 14:31:40 2017

@author: kdalal
"""

import requests
import simplejson
import pandas as pd
import re

#Importing all country codes
con_code = pd.read_csv("country.csv",header=None)
con_code = con_code.ix[:,0].tolist()

#Defining request header
header = {'Accept':'application/json, text/javascript, */*; q=0.01',
          'Accept-Encoding':'gzip, deflate, br',
          'Accept-Language':'en-US,en;q=0.8',
          'Connection':'keep-alive',
          'Content-Length':'505',
          'Content-Type':'application/json',
          'Cookie':'hpe_locale={%22country%22:%22us%22%2C%22language%22:%22en%22}; hpeuck_answ=0; BKUT=1492635175; AMCVS_56B5A25055667EED7F000101%40AdobeOrg=1; AMCV_56B5A25055667EED7F000101%40AdobeOrg=2096510701%7CMCIDTS%7C17284%7CMCMID%7C63809289832038373232160916792206955923%7CMCAAMLH-1493906425%7C9%7CMCAAMB-1493990951%7CNRX38WO0n5BH8Th-nqAG_A%7CMCOPTOUT-1493393351s%7CNONE%7CMCAID%7CNONE%7CMCSYNCSOP%7C411-17291%7CvVersion%7C2.0.0; ELOQUA=GUID=18fdf18a8d5e4d219070603e83336771; HP_LB=0; Authsite=httpss%3A%2F%2Fwww.google.com%2F; AppKey=NONE; W2GISM=46f5008dbf7c9449b376fe81ea12bd8a; bkreqmade=1; s_cc=true; hpeuck_cktst=1; _bizo_bzid=1751eb69-5f7f-48de-9377-e277d3f14412; _bizo_cksm=5F531A941855972E; _bizo_np_stats=14%3D120%2C; utag_main=v_id:015b87fd4457001c68ac75d1752a0406c001d06b00ac2$_sn:13$_ss:0$_st:1493390522191$_pn:2%3Bexp-session$ses_id:1493386150285%3Bexp-session$_prevpage:PartnerLocator%3ALocatorPage%3Bexp-1493392322165; lskv_dceventonce=%2F%3Ana; s_sq=hpcstsg%3D%2526c.%2526a.%2526activitymap.%2526page%253DPartnerLocator%25253ALocatorPage%2526link%253DSEARCH%2526region%253Dlocator_search%2526pageIDType%253D1%2526.activitymap%2526.a%2526.c%2526pid%253DPartnerLocator%25253ALocatorPage%2526pidt%253D1%2526oid%253DSEARCH%2526oidt%253D3%2526ot%253DSUBMIT',
          'Host':'findapartner.hpe.com',
          'Origin':'https://findapartner.hpe.com',
          'Referer':'https://findapartner.hpe.com/',
          'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36',
          'X-Requested-With':'XMLHttpRequest'}

#Scraping data for entire US, state-wise in order to overcome content-limit 
response=[]
c = []
states = ['CA','TX','NY','FL','IL','MA','VA','PA','NJ','GA','MD','OH','MI','CO','MN','NC','WI','AZ','IN','MO','CT','WA','TN','KS','LA','UT','IA','OR','SC','AL','KY','NE','NH',
          'NV','OK']

for i in states:
    payload = {
      "request": {
        "appkey": "936725A4-7D9A-11E5-81AC-86EC8D89CD5A",
        "formdata": {
          "dataview": "store_default",
          "limit": 250,
          "geolocs": {
            "geoloc": [
              {
                "addressline": "",
                "country": "US",
                "latitude": "",
                "longitude": "",
                "state": i,
                "province": "",
                "city": "",
                "address1": "",
                "postalcode": ""
              }
            ]
          },
          "searchradius": "15|25|50|100|250|350|450|550|650|750|850|950",
          "nobf": 1,
          "where": {
            "partner_service_prov": {
              "eq": 1
            }
          },
          "google_autocomplete": 'true',
          "currency": "US",
          "distinct": "_distance, clientkey",
          "true": "1"
        }
      }
    }
    print payload['request']['formdata']['geolocs']['geoloc']
    r = requests.post("https://findapartner.hpe.com/rest/locatorsearch?like=0.9847125597637671",data=simplejson.dumps(payload),headers=header)
    result = simplejson.loads(str(r.content))
    response.append(result['response'])

collection = [i['collection'] for i in response]
for i in collection:
    c = c + i

# Scraping all countries other than US:
def other_countries(code,country,partner_type):
    
    url = "https://findapartner.hpe.com/rest/getlist?lang=en_US&like=" + code
    
    payload = {
          "request": {
            "appkey": "936725A4-7D9A-11E5-81AC-86EC8D89CD5A",
            "formdata": {
              "objectname": "Locator::Base",
              "currency": country,
              "where": {
                "country": {
                  "eq": country
                },
                "parent_partner_id": {
                  "eq": "null"
                },
                partner_type: {
                  "eq": 1
                }
              },
              "order": "partner_ranking != 'PLATINUM', partner_ranking != 'GOLD', partner_ranking != 'SILVER', partner_ranking != 'BUSINESS'",
              "limit": 250
            }
          }
        }
    print payload['request']['formdata']['where']
    r = requests.post(url,data=simplejson.dumps(payload),headers=header)
    results = simplejson.loads(str(r.content))
    return results['response']

rest=[]
a = "partner_reseller"
b = "partner_service_prov"
c= "partner_auth_suprt_prov"

for i in con_code:
    try:
        rest.append(other_countries("0.6706142146058582",i,a))
        rest.append(other_countries("0.6706142146058582",i,b))
        rest.append(other_countries("0.6706142146058582",i,c))
    except Exception:
        pass 

temp=[i.get('collection') for i in rest]
not_none=[x for x in temp if x is not None]
temp1=[]
for i in not_none:
    temp1=temp1+i
len(temp1)
combine = temp1 + c

#Loading all scraped data in the dataframe
company = [i['name'] for i in combine]
partner_id = [i['uid'] for i in combine]
website = [i['partner_website'] for i in combine]
address1 = [i['address1'] for i in combine]
city = [i['city'] for i in combine]
state = [i['state'] for i in combine]
country = [i['country'] for i in combine]
zipcode = [i['postalcode'] for i in combine]
contact_email = [i['partner_email'] for i in combine]
phone_number = [i['phone'] for i in combine]
phone_number = [re.sub("[^0-9]","",i.encode('utf-8')) if i else i for i in phone_number]
fax = [i['fax'] for i in combine]
fax = [re.sub("[^0-9]","",i.encode('utf-8')) if i else i for i in fax ]
support_provider = [i['partner_auth_suprt_prov'] for i in combine]
partner_reseller = [i['partner_reseller'] for i in combine]
partner_service_provider = [i['partner_service_prov'] for i in combine]

hpe_df = pd.DataFrame(company,columns=['company'])
hpe_df['partner_id'] = partner_id
hpe_df['website'] = website
hpe_df['address1'] = address1
hpe_df['city'] = city
hpe_df['state'] = state
hpe_df['zipcode'] = zipcode
hpe_df['country'] = country
hpe_df['contact_email'] = contact_email
hpe_df['phone_number'] = phone_number
hpe_df['fax'] = fax  
hpe_df['support_provider'] = support_provider
hpe_df['reseller'] = partner_reseller
hpe_df['service_provider'] = partner_service_provider
hpe_df.to_csv("hpe.csv", encoding='utf-8')
      

