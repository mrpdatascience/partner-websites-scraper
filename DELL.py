# -*- coding: utf-8 -*-
"""
Created on Wed May 03 11:12:04 2017

@author: kdalal
"""
import requests
import simplejson
import pandas as pd

header = {
        'Accept':'application/json, text/plain, */*',
        'Accept-Encoding':'gzip, deflate, br',
        'Accept-Language':'en-US,en;q=0.8',
        'Connection':'keep-alive',
        'Content-Length':'45',
        'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8',
        'Cookie':'AMCVS_A6F4776A5245B0EA0A490D44%40AdobeOrg=1; s_dnb=true; AMCVS_4DD80861515CAB990A490D45%40AdobeOrg=1; AMCV_A6F4776A5245B0EA0A490D44%40AdobeOrg=2096510701%7CMCMID%7C64023338185466419402137287782245514252%7CMCAAMLH-1494427098%7C9%7CMCAAMB-1494427098%7CNRX38WO0n5BH8Th-nqAG_A%7CMCOPTOUT-1493829498s%7CNONE%7CMCAID%7CNONE%7CMCSYNCSOP%7C411-17297%7CvVersion%7C2.0.0; AMCV_4DD80861515CAB990A490D45%40AdobeOrg=-1176276602%7CMCIDTS%7C17290%7CMCMID%7C58870677072583886141496201843929817246%7CMCAAMLH-1494427099%7C9%7CMCAAMB-1494427099%7CNRX38WO0n5BH8Th-nqAG_A%7CMCOPTOUT-1493829499s%7CNONE%7CMCAID%7CNONE; rumCki=false; s_depth=1; sessionTime=2017%2C4%2C3%2C10%2C38%2C19%2C934; mbox=session#1493822297980-131359#1493824293|PC#1493822297980-131359.20_58#1501598433; s_dfa=emc-ngoe-perf; PREFLANG=en-us; LASTEMCDOMAIN=emc.com; _ga=GA1.2.1987017487.1493822300; _gid=GA1.2.160882768.1493822437; gpv_pn=emc.com%2Fpartners%2Fpartner-finder%2Fsearch; s_cc=true; LPCKEY-48454298=7c5d91d1-3b5e-4761-808a-e7a78dc5f91c8-60875%7Cnull%7Cnull%7C40; LPVID=FmYTcwYWUyZWE3NTU5ZDlj; LPSID-48454298=Wuw6jq03QBiat7Yz0deYPg; _bizo_bzid=1751eb69-5f7f-48de-9377-e277d3f14412; _bizo_cksm=261B65E01F070154; _bizo_np_stats=155%3D211%2C; s_ppvl=partners%2Fpartner-finder%2Fsearch.htm%2C100%2C64%2C1000%2C814%2C638%2C1366%2C768%2C1%2CP; _gali=closetabsbutton; s_ppv=partners%2Fpartner-finder%2Fsearch.htm%2523%2F%253Fcountry%253DUNITED_STATES%2526button%253D0%2526startIndex%253D51%2C100%2C100%2C1000%2C814%2C638%2C1366%2C768%2C1%2CP; s_visit=1; s_nr=1493823036756-New; c=undefinedwww.google.comwww.google.com; s_ppn=partners%2Fpartner-finder%2Fsearch.htm%23%2F%3Fcountry%3DUNITED_STATES%26button%3D0%26startIndex%3D51; cidlid=%3A%3A; s_dl=1; s_channelstack=%5B%5B%27Referrers%27%2C%271493823033485%27%5D%2C%5B%27Referrers%27%2C%271493823033650%27%5D%2C%5B%27Referrers%27%2C%271493823033679%27%5D%2C%5B%27Referrers%27%2C%271493823033704%27%5D%2C%5B%27Referrers%27%2C%271493823036767%27%5D%5D; s_hwp=null%7C%7Cnull%7C%7C3%3A5%3A2017%3A10%3A50%7C%7CN%7C%7CN%7C%7Cnull%7C%7C12%7C%7Cnull%7C%7Cnull%7C%7CN%7C%7Cnull%7C%7Cnull%7C%7Cnull; s_opv=us%3Acorp%7Cevent87',
        'Host':'www.emc.com',
        'Origin':'https://www.emc.com',
        'Referer':'https://www.emc.com/partners/partner-finder/search.htm',
        'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36'
        }

countries = ["Albania","Angola","Algeria","Argentina","Belize","Bermuda","Bolivia","BOSNIA_AND_HERZEGOVINA","Brazil","Canada","Chile","BOSNIA_AND_HERZEGOVINA","Colombia","Costa_Rica","DOMINICAN_REPUBLIC", "Ecuador","Guatemala","Honduras","Mexico","Nicaragua","Paraguay","Peru","Puerto_Rico",
             "United_States","EL_SALVADOR","Uruguay","Venezuela","Australia","Cambodia","China","Hong_Kong","India","Indonesia","Japan","Nepal","New_Zealand","Philippines","Singapore","KOREA,_REPUBLIC_OF",
             "Sri_Lanka","Taiwan","PALESTINIAN_TERRITORY,_OCCUPIED", "Province_of_China","Thailand","Belgium","Denmark","Estonia","Finland","France","Germany","Gibraltar","Greece","Iceland","Ireland","ISLE_OF_MAN","Israel","MACEDONIA,_REPUBLIC_OF"
             "Italy","Latvia","Lithuania","Luxembourg","Netherlands","Norway","Portugal","Spain","Sweden","Switzerland","United_Kingdom","Pakistan","Panama","Paraguay","Phillipines","Qatar","Russian_federation","Romania","Nigeria","VIET_NAM",
             "Uruguay","Zambia","UNITED_ARAB_EMIRATES","Belgium","cyprus","egypt","hungary","iceland","cameroon","Colombia","IRAQ","Ireland","Indonesia"]

countries = [i.upper() for i in countries]

def scrape(button,country):
    
    response = []        
    for r in range(0,1000,50):
        if r==0:
            payload = {
                    "country":country,
                    "button":button,
                    "startIndex":r
                    }
        else:
            payload = {
                    "country":country,
                    "button":button,
                    "startIndex":r+1
                    }
        result = requests.post("https://www.emc.com/partners/partner-finder/getPartnersByFilterAndOrLocation.htm",data=payload,headers=header)
        r = simplejson.loads(str(result.content))
        print payload
        print r["partners"][0]
        response.append(r["partners"])
        return response

output=[]
for i in [0,1,2]:
    for j in countries:
        try:
            output.append(scrape(i,j))
            len(output)
        except Exception:
            pass
        
p = []
for i in output:
    p += i

c=[]
for j in p:
    c+=j

company = [i.get("partner_name") for i in c]
partnerId = [i.get("partner_id") for i in c] 
address1 = [(i["location"] or {}).get('street_1') for i in c]
city = [(i["location"] or {}).get('city') for i in c]
zipcode = [(i["location"] or {}).get('zip') for i in c]
state = [(i["location"] or {}).get('state') for i in c]
country = [(i["location"] or {}).get('country') for i in c]
website = [(i["url_to_website"] or None) for i in c]
contact_number = [(i["location"] or {}).get('phone') for i in c]
contact_person = [(i["location"] or {}).get('point_of_contact') for i in c]
partner_type = [i["partner_type"] for i in c]

dell_df = pd.DataFrame(company,columns=["company"])
dell_df['partner_id'] = partnerId
dell_df['address1'] = address1
dell_df['city'] = city
dell_df['zipcode'] = zipcode
dell_df['state'] = state
dell_df['country'] = country
dell_df['website'] = website
dell_df['contact_number'] = contact_number
dell_df['contact_person'] = contact_person
dell_df['partner_type'] = partner_type
dell_df.to_csv('dell.csv', encoding='utf-8')      
